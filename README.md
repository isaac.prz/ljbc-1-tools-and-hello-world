# LJBC-1 tools and hello world

Learn java by challenge lesson 1: tools and hello world 
This is the first lesson, learn how to configure the tools so you can start programming for the next lessons. Additionally we will write our own hello world program.

## Challenge
This is the first challenge, goal, checkout this project, integrate it in your own IDE (for instance Eclipse) and run your first java program.

## Instructions

### Configure Git
 Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

 * First of all create a new account on https://gitlab.com/
 * The go to *Edit Profile* => *SSH KEYS* and follow the instructions to generate and add your own ssh key. This will allow you access into gitlab.
 * Then install a [git client](https://git-scm.com/downloads) on your computer 
 * use `git --version` to validate if the installation was correctly done
 
### Check out this repository
 * Create a new folder to serve as future workspace. For instance learn-java-by-challenge
 * Clone this project `git clone git@gitlab.com:isaac.prz/ljbc-1-tools-and-hello-world.git`
 * once done check that you got a new folder *ljbc-1-tools-and-hello-world* with the contents of this repository
 
### Install a Java JDK

The JDK allows developers to create Java programs that can be executed and run by java.

 * there are different java distributions, but we will pick up [zulu-17](https://www.azul.com/downloads/?package=jdk#download-openjdk), if you have already one on this version, feel free to use it.
 * once install open an command line and write `javac -version` to check if you got the correct version

### Create, compile and Run the HelloWorld Class
 * Create a new text file called HelloWorld.java (please be sure that you dont have the .txt extension)

```
class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World!"); 
    }
}

```

 * compile it with `javac HelloWorld.java`
 * execute it with `java HelloWorld`
 
### Do it on your IDE
Install your IDE (integrated development environment) you have several, just pick one and stick with it:
* [Eclipse IDE for Java Developers](https://www.eclipse.org/downloads/packages/release/2021-12/r/eclipse-ide-java-developers). Recommended for this course
* [Intellij Community Edition](https://www.jetbrains.com/idea/download/#section=windows)
* [Netbeans](https://netbeans.apache.org/download/index.html)

afterwards, create or integrate this folder as a new java project. And try to run the HelloWorld on your IDE (right click over the class and *run as java application*).

![Run hello world](docs/run-hello-world.png)

### Explanation

Once you run the program you will see down on the Console the word: `Hello, World!`:
 * the Console is the place where you can input your data or output what you want to print.
 
The `public static void main(String[] args)` is the method that will be executed once you run this class. All the instruccions you run there there will be executed.

   `System.out.println` is used to print a line in the console. The system console in this case is represented by `System`, and ``System.out` represents the "standard" output stream.


## Overall Information about  _Learn java by challenge_
 This is a set of projects or challeges, ordered in order to serve as a lesson. Every lesson will progress you into your goal, learning java with focus on enterprise practises. 

## Contributing
 Feel free to contribute any commentary, constructive critique, or PR with proposed changes is wellcome

## Authors and acknowledgment
 * Isaac Perez (main author)

## License

Copyright 2021 Isaac Perez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More information [MIT License](https://opensource.org/licenses/MIT)



